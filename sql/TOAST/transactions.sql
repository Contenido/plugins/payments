create table payments_transactions
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	dtime timestamp not null default now(),
	status smallint not null default 0,
	provider text,
	name text,
	account_id numeric,
	session_id text,
	order_id integer not null,
	operation_id numeric not null,
	currency_code varchar(4),
	sum float,
	account_user text,
	payment_system text,
	account_corr text,
	success smallint default 1,
	data text
);
CREATE INDEX payments_transactions_operations ON payments_transactions USING btree (provider, operation_id);
CREATE INDEX payments_transactions_sessions ON payments_transactions USING btree (provider, session_id) WHERE session_id is not null;
CREATE INDEX payments_transactions_orders ON payments_transactions USING btree (order_id);
