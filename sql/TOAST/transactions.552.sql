ALTER TABLE payments_transactions ADD COLUMN session_id text;
ALTER TABLE payments_transactions ADD COLUMN success smallint default 1;
CREATE INDEX payments_transactions_sessions ON payments_transactions USING btree (provider, session_id) WHERE session_id is not null;
