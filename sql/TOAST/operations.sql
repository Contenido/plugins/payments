create table payments_operations
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	status smallint not null default 0,
	name text,
	order_id integer not null,
	uid integer not null,
	uuid integer,
        sum float,
	data text
);
CREATE INDEX payments_operations_order ON payments_operations USING btree (order_id);
