create table payments_cards
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	status smallint not null default 0,
	uid integer not null,
	type integer not null,
	number text,
	name text,
        month integer not null,
	year integer not null,
	cvv_cvc text,
	sections integer not null,
	data text
);
CREATE INDEX payments_cards_number ON payments_cards USING btree (number);
