alter table payments_operations add column uuid integer;
CREATE INDEX payments_operations_uuid ON payments_operations USING btree (uuid) where uuid is not null;
