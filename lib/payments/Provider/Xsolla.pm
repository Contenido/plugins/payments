package payments::Provider::Xsolla;

use strict;
use warnings 'all';

use base 'payments::Provider::Base';
use Contenido::Globals;
use payments::Keeper;
use Digest::MD5;
use Data::Dumper;
use Net::Subnet;

sub get_iframe {
    my $self = shift;
    my (%opts) = @_;
    my $id = delete $opts{id};
    return unless $id;
    my $sum = delete $opts{sum};
    return unless $sum;

    $sum = $sum =~ /\d+\.\d{2}/ ? $sum : sprintf( "%.2f", $sum );
    my @static_params = (
#	'action=directpayment',
	'pid=26',
	'marketplace=paydesk',
	'special_skip_step=0',
	'prime=26,16,27,6,64',
	'other=1',
	'limit=5',
    );
    my @sig_params = (
	'out='.($opts{out} || '100.00'),
	'project='.$state->{payments}{xsolla_app_id},
	'theme='.($opts{theme} || 115),
	'v1='.$id,
    );
    push @sig_params, 'currency='.$opts{currency}	if exists $opts{currency};
    push @sig_params, 'email='.$opts{email}		if exists $opts{email};
    push @sig_params, 'userip='.$opts{userip}		if exists $opts{userip};
    push @sig_params, 'v2='.$opts{v2}			if exists $opts{v2};
    push @sig_params, 'v3='.$opts{v3}			if exists $opts{v3};
    @sig_params = sort { $a cmp $b } @sig_params;
    my $str = join( '', @sig_params).$self->secret;
    warn "Sign str: [$str]\n";
    my $sign = Digest::MD5::md5_hex ( $str );
    my $iframe = '<iframe id="paystation" src="https://secure.xsolla.com/paystation2/?'.join('&',@sig_params, 'sign='.$sign, @static_params).'" width="100%" height="100%">';
    return $iframe;
}


1;