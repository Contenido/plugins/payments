package payments::Provider::Moneta;

use strict;
use warnings 'all';

use base 'payments::Provider::Base';
use Contenido::Globals;
use payments::Keeper;
use Digest::MD5;
use Data::Dumper;

sub get_form {
    my $self = shift;
    my (%opts) = @_;
    my $id = delete $opts{id};
    return unless $id;
    my $sum = delete $opts{sum};
    return unless $sum;

    $sum = $sum =~ /\d+\.\d{2}/ ? $sum : sprintf( "%.2f", $sum );

    my %fields = (
	'method'	=> 'post',
	'action'	=> 'https://www.moneta.ru/assistant.htm',
	'visible'	=> [
		{ type => 'submit', value => $opts{submit} || 'Оплатить' },
	],
        'hidden'	=> [
		{ name => 'MNT_ID', value => $state->{payments}{moneta_app_id} },
		{ name => 'MNT_TRANSACTION_ID', value => $id },
		{ name => 'MNT_CURRENCY_CODE', value => $state->{payments}{moneta_currency_code} },
		{ name => 'MNT_AMOUNT', value => $sum },
	],
    );
    if ( $state->{payments}{moneta_test_mode} ) {
	push @{$fields{hidden}}, { name => 'MNT_TEST_MODE', value => 1 }
    }
    if ( $opts{success} ) {
	push @{$fields{hidden}}, { name => 'MNT_SUCCESS_URL', value => $opts{success} }
    }
    if ( $opts{fail} ) {
	push @{$fields{hidden}}, { name => 'MNT_FAIL_URL', value => $opts{fail} }
    }
    if ( $state->{payments}{moneta_sig_code} ) {
	my $str = $state->{payments}{moneta_app_id}.$id.$sum.$state->{payments}{moneta_currency_code}.$state->{payments}{moneta_test_mode}.$state->{payments}{moneta_sig_code};
	my $md5 = Digest::MD5::md5_hex ( $str );
	push @{$fields{hidden}}, { name => 'MNT_SIGNATURE', value => $md5 }
    }

    if ( exists $opts{custom1} ) {
	push @{$fields{hidden}}, { name => 'MNT_CUSTOM1', value => $opts{custom1} }
    }
    if ( exists $opts{custom2} ) {
	push @{$fields{hidden}}, { name => 'MNT_CUSTOM2', value => $opts{custom2} }
    }
    if ( exists $opts{custom3} ) {
	push @{$fields{hidden}}, { name => 'MNT_CUSTOM3', value => $opts{custom3} }
    }

    return \%fields;
}


1;