package payments::Card;

use base "Contenido::Document";
sub extra_properties
{
	return (
		{ 'attr' => 'status',   'type' => 'status',     'rusname' => 'Статус карты оплаты',
			'cases' => [
					[0, 'Карта не активна'],
					[1, 'Карта активна'],
                                        [2, 'Истек срок действия карты'],
                                        [3, 'Карта заблокирована'],
				],
		},
		{ 'attr' => 'type',     'type' => 'status',	'rusname' => 'Тип карты оплаты',
                         'cases' => [
                                        [0, 'MasterCard'],
                                        [1, 'Visa'], 
                                ],
                },
	)
}

sub class_name
{
	return 'Payments: Банковская карта';
}

sub class_description
{
	return 'Payments: Карта';
}

sub class_table
{
	return 'payments::SQL::CardsTable';
}

1;
