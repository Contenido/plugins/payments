package payments::Keeper;

use strict;
use warnings 'all';
use base qw(Contenido::Keeper);


use Contenido::Globals;

sub add {
    my $self = shift;
    my (%opts) = @_;

    return undef	unless $opts{type} && ( $opts{order} && ref $opts{order} || $opts{uid} && $opts{order} && $opts{sum});
    return undef	unless $opts{type} =~ /^(create|suspend|cancel|append|refund|close)$/;

    my $status = exists $opts{status} ? $opts{status} : $state->development;
    my $ops = $keeper->get_documents(
		class		=> 'payments::Operation',
		status		=> $status,
		order_id	=> ref $opts{order} ? $opts{order}->id : $opts{order},
		order_by	=> 'ctime',
		return_mode	=> 'array_ref',
	);
    my $new = 0;
    my $op;
    if ( ref $ops eq 'ARRAY' && @$ops ) {
	my $last = $ops->[-1];
	if ( $opts{type} eq 'create' && ($last->name eq 'suspend' || $last->name eq 'cancel' || $last->name eq 'close') ) {
		warn "PAYMENTS[add-op]: Заказ закрыт, отменен или заморожен. Оплата по нему невозможна\n";
		return undef;
	} elsif ( $opts{type} eq 'refund' && ($last->name eq 'suspend' || $last->name eq 'close') ) {
		warn "PAYMENTS[add-op]: Заказ закрыт или заморожен. Возврат средств по нему невозможен\n";
		return undef;
	} elsif ( $last->name eq $opts{type} ) {
		$op = $last;
	}
    } elsif ( $opts{type} eq 'create' ) {
	$new = 1;
    } else {
	warn "PAYMENTS[add-op]: Попытка создать операцию ['.$opts{type}.'] без регистрации типа [create]\n";
	return undef;
    }
    if ( $new ) {
	$op = payments::Operation->new( $keeper );
	$op->status( $status );
	$op->name( $opts{type} );
	if ( ref $opts{order} ) {
		$op->uid( $opts{order}->uid );
		$op->order_id( $opts{order}->id );
		$op->sum( $opts{order}->sum_total );
	} else {
		$op->uid( $opts{uid} );
		$op->order_id( $opts{order} );
		$op->sum( $opts{sum} );
	}
	if ( exists $opts{uuid} && $opts{uuid} ) {
		$op->uuid( $opts{uuid} );
	}
	$op->store;
    }
    return $op;
}


sub check {
    my $self = shift;
    my $order_id = shift;
    return undef	unless $order_id;

    my $ops = $keeper->get_documents(
		class   => 'payments::Operation',
		order_id        => $order_id,
		order_by        => 'ctime',
		return_mode     => 'array_ref',
	);
    if ( ref $ops eq 'ARRAY' && @$ops ) {
	return $ops->[-1];
    }
    return undef;
}

sub get_order {
    my $self = shift;
    my $order_id = shift;
    return undef	unless $order_id;

    if ( grep { $_ eq 'webshop' } split(/\s+/, $state->plugins) ) {
	return $keeper->{webshop}->get_orders( id => $order_id, list => 1 );
    }
}

1;
