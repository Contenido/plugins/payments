package payments::Init;

use strict;
use warnings 'all';

use Contenido::Globals;
use payments::Apache;
use payments::Keeper;

# загрузка всех необходимых плагину классов
# payments::SQL::SomeTable
# payments::SomeClass
Contenido::Init::load_classes(qw(
		payments::SQL::TransactionsTable
		payments::Transaction

		payments::SQL::OperationsTable
		payments::Operation

                payments::SQL::CardsTable
	        payments::Card

		payments::TransactionSection

                payments::CardSection

		payments::Provider::Base
		payments::Provider::PayTure
		payments::Provider::Sber
	));

sub init {
	push @{ $state->{'available_documents'} }, qw( payments::Transaction payments::Operation payments::Card );
	push @{ $state->{'available_sections'} }, qw( payments::TransactionSection payments::CardSection );
	0;
}

1;
