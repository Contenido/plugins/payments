package payments::Apache;

use strict;
use warnings 'all';

use payments::State;
use Contenido::Globals;


sub child_init {
	# встраиваем keeper плагина в keeper проекта
	$keeper->{payments} = payments::Keeper->new($state->payments);
}

sub request_init {
}

sub child_exit {
}

1;
