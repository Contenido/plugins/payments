package payments::Operation;

use base "Contenido::Document";
sub extra_properties
{
	return (
		{ 'attr' => 'name',	'type' => 'status',	'rusname' => 'Тип операции',
			'cases' => [
					['create', 'создан'],
					['suspend', 'заморожен'],
					['resume', 'активирован'],
					['reform', 'изменен'],
					['append', 'доплата'],
					['cancel', 'отменен'],
					['close', 'закрыт'],
				],
		},
	)
}

sub class_name
{
	return 'Payments: операция с заказом';
}

sub class_description
{
	return 'Payments: операция с заказом';
}

sub class_table
{
	return 'payments::SQL::OperationsTable';
}

1;
