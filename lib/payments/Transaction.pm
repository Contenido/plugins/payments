package payments::Transaction;

use base "Contenido::Document";
sub extra_properties
{
	return (
		{ 'attr' => 'status',   'type' => 'status',     'rusname' => 'Статус тестирования',
			'cases' => [
					[0, 'Реальная оплата'],
					[1, 'Тестовая оплата'],
				],
		},
		{ 'attr' => 'form_url',		'type' => 'string',	'rusname' => 'URL формы оплаты' },
		{ 'attr' => 'custom1',		'type' => 'string',	'rusname' => 'Параметр 1' },
		{ 'attr' => 'custom2',		'type' => 'string',	'rusname' => 'Параметр 2' },
		{ 'attr' => 'custom3',		'type' => 'string',	'rusname' => 'Параметр 3' },
	)
}

sub class_name
{
	return 'Payments: транзакция';
}

sub class_description
{
	return 'Payments: транзакция';
}

sub class_table
{
	return 'payments::SQL::TransactionsTable';
}

1;
