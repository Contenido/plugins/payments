package payments::SQL::CardsTable;

use base 'SQL::DocumentTable';

sub db_table
{
	return 'payments_cards';
}


sub available_filters {
	my @available_filters = qw(

					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
				        _class_excludes_filter
					_sfilter_filter
					_excludes_filter
					_s_filter

					_uid_filter
					_number_filter
			);

	return \@available_filters;
}

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
	my $self = shift;

	my @parent_properties = grep { $_->{attr} ne 'sections' } $self->SUPER::required_properties;
	return (
		@parent_properties,
		{
			'attr'		=> 'uid',
			'type'		=> 'integer',
			'rusname'	=> 'Уник профиля пользователя',
			'db_field'      => 'uid',
			'db_type'       => 'integer',
			'db_opts'       => "not null",
		},
		{
			'attr'		=> 'number',
			'type'		=> 'string',
			'rusname'	=> 'Номер карты',
			'db_field'   	=> 'number',
			'db_type'	=> 'text',
		},
		{	
			'attr'		=> 'month',
			'type'		=> 'integer',
			'rusname'   	=> 'Месяц окончания действия карты',
			'db_field'      => 'month',
			'db_type'	=> 'integer',
			'db_opts'   	=> "not null",
		},
		{	
			'attr'		=> 'year',
			'type'		=> 'integer',
			'rusname'   	=> 'Год окончания действия карты',
			'db_field'	=> 'year',
			'db_type'	=> 'integer',
			'db_opts'   	=> "not null",
		},
		{
			'attr'		=> 'cvv_cvc',
			'type'		=> 'string',
			'rusname'	=> 'Код проверки',
			'db_field'   	=> 'cvv_cvc',
			'db_type'	=> 'text',
		},
		{	
			'attr'		=> 'type',
			'type'		=> 'integer',
			'rusname'   	=> 'Тип платежной системы',
			'db_field'	=> 'type',
			'db_type'	=> 'integer',
			'db_opts'   	=> "not null",
		},
		{	
			'attr'		=> 'sections',
			'type'		=> 'sections_list',
			'rusname'   	=> 'Секции',
			'hidden'	=> 1,
			'db_field'	=> 'sections',
			'db_type'	=> 'integer',
		},
	);
}


########### FILTERS DESCRIPTION ###############################################################################
sub _s_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{s} );
	return &SQL::Common::_generic_int_filter('d.sections', $opts{s});
}

sub _uid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{uid} );
	return &SQL::Common::_generic_int_filter('d.uid', $opts{uid});
}

sub _number_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{number} );
	return &SQL::Common::_generic_text_filter('d.number', $opts{number});
}

1;