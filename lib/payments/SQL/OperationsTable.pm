package payments::SQL::OperationsTable;

use base 'SQL::DocumentTable';

sub db_table
{
	return 'payments_operations';
}


sub available_filters {
	my @available_filters = qw(

					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
				        _class_excludes_filter
					_sfilter_filter
					_excludes_filter
					_datetime_filter
					_date_equal_filter
					_date_filter
					_previous_days_filter

					_order_id_filter
					_uid_filter
			);

	return \@available_filters;
}

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
	my $self = shift;

	my @parent_properties = grep { $_->{attr} ne 'dtime' && $_->{attr} ne 'sections' } $self->SUPER::required_properties;
	return (
		@parent_properties,
		{
			'attr'		=> 'uid',
			'type'		=> 'integer',
			'rusname'	=> 'ID пользователя',
			'db_field'      => 'uid',
			'db_type'       => 'integer',
			'db_opts'       => "default 0",
		},
		{
			'attr'		=> 'uuid',
			'type'		=> 'integer',
			'rusname'	=> 'ID менеджера',
			'db_field'      => 'uuid',
			'db_type'       => 'integer',
		},
		{						       # ID заказа
			'attr'		=> 'order_id',
			'type'		=> 'integer',
			'rusname'	=> 'ID заказа',
			'db_field'	=> 'order_id',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null",
		},
		{
			'attr'		=> 'sum',
			'type'		=> 'string',
			'rusname'	=> 'Сумма',
			'db_field'	=> 'sum',
			'db_type'	=> 'float',
		},
	);
}


########### FILTERS DESCRIPTION ###############################################################################
sub _uid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{uid} );
	return &SQL::Common::_generic_int_filter('d.uid', $opts{uid});
}

sub _order_id_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{order_id} );
	return &SQL::Common::_generic_int_filter('d.order_id', $opts{order_id});
}

1;