package payments::SQL::TransactionsTable;

use base 'SQL::DocumentTable';

sub db_table
{
	return 'payments_transactions';
}


sub available_filters {
	my @available_filters = qw(

					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
				        _class_excludes_filter
					_sfilter_filter
					_excludes_filter
					_datetime_filter
					_date_equal_filter
					_date_filter
					_previous_days_filter

					_operation_id_filter
					_order_id_filter
					_session_id_filter
					_success_filter
					_provider_filter
					_name_exact_filter
			);

	return \@available_filters;
}

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
	my $self = shift;

	my @parent_properties = grep { $_->{attr} ne 'sections' } $self->SUPER::required_properties;
	return (
		@parent_properties,
		{
			'attr'		=> 'provider',
			'type'		=> 'string',
			'rusname'	=> 'Провайдер',
			'db_field'      => 'provider',
			'db_type'       => 'text',
		},
		{
			'attr'		=> 'account_id',
			'type'		=> 'integer',
			'rusname'	=> 'Аккаунт',
			'db_field'      => 'account_id',
			'db_type'       => 'numeric',
			'db_opts'       => "default 0",
		},
		{
			'attr'		=> 'session_id',
			'type'		=> 'string',
			'rusname'	=> 'Ключ сессии',
			'db_field'      => 'session_id',
			'db_type'       => 'text',
		},
		{						       # ID заказа
			'attr'		=> 'order_id',
			'type'		=> 'integer',
			'rusname'	=> 'ID заказа',
			'db_field'	=> 'order_id',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null",
		},
		{						       # ID операции в таблице операций
			'attr'		=> 'operation_id',
			'type'		=> 'integer',
			'rusname'	=> 'ID операции',
			'db_field'	=> 'operation_id',
			'db_type'	=> 'numeric',
			'db_opts'	=> "not null",
		},
		{
			'attr'		=> 'currency_code',
			'type'		=> 'string',
			'rusname'	=> 'ID валюты',
			'db_field'	=> 'currency_code',
			'db_type'	=> 'varchar(4)',
		},
		{
			'attr'		=> 'sum',
			'type'		=> 'string',
			'rusname'	=> 'Сумма платежа',
			'db_field'	=> 'sum',
			'db_type'	=> 'float',
		},
		{
			'attr'		=> 'account_user',
			'type'		=> 'string',
			'rusname'	=> 'Номер счета пользователя',
			'db_field'	=> 'account_user',
			'db_type'	=> 'text',
		},
		{
			'attr'		=> 'account_corr',
			'type'		=> 'string',
			'rusname'	=> 'Номер счета плательщика',
			'db_field'	=> 'account_corr',
			'db_type'	=> 'text',
		},
		{
			'attr'		=> 'payment_system',
			'type'		=> 'string',
			'rusname'	=> 'Идентификатор платежной системы',
			'db_field'	=> 'payment_system',
			'db_type'	=> 'text',
		},
		{						       # Результат транзакции
			'attr'		=> 'success',
			'type'		=> 'checkbox',
			'rusname'	=> 'Транзакция прошла успешно',
			'db_field'	=> 'success',
			'db_type'	=> 'smallint',
			'db_opts'	=> "default 1",
		},
	);
}


########### FILTERS DESCRIPTION ###############################################################################
sub _operation_id_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{operation_id} );
	return &SQL::Common::_generic_int_filter('d.operation_id', $opts{operation_id});
}

sub _order_id_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{order_id} );
	return &SQL::Common::_generic_int_filter('d.order_id', $opts{order_id});
}

sub _success_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{success} );
	return &SQL::Common::_generic_int_filter('d.success', $opts{success});
}

sub _provider_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{provider} );
	return &SQL::Common::_generic_text_filter('d.provider', $opts{provider});
}

sub _session_id_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{session_id} );
	return &SQL::Common::_generic_int_filter('d.session_id', $opts{session_id});
}

sub _name_exact_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{name_exact} );
	return &SQL::Common::_generic_text_filter('d.name', $opts{name_exact});
}

1;