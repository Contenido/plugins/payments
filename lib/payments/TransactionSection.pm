package payments::TransactionSection;

use base 'Contenido::Section';

sub extra_properties
{
	return (
		{ 'attr' => 'brief',    'type' => 'text',       'rusname' => 'Описание секции' },
		{ 'attr' => 'default_document_class',		'default' => 'payments::Transaction' },
		{ 'attr' => '_sorted',				'hidden' => 1 },
		{ 'attr' => 'order_by',				'hidden' => 1 },
	)
}

sub class_name
{
	return 'Payments: Секция транзакций';
}

sub class_description
{
	return 'Payments: Секция транзакций';
}

1;
