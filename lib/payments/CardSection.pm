package payments::CardSection;

use base 'Contenido::Section';

sub extra_properties
{
	return (
		{ 'attr' => 'brief',    'type' => 'text',       'rusname' => 'Описание секции' },
		{ 'attr' => 'default_document_class',		'default' => 'payments::Card' },
		{ 'attr' => '_sorted',				'hidden' => 1 },
		{ 'attr' => 'order_by',				'hidden' => 1 },
	)
}

sub class_name
{
	return 'Payments: Секция карт';
}

sub class_description
{
	return 'Payments: Секция карт';
}

1;
